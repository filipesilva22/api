# Demo instructions:
#### - Install all dependencies: composer install

#### - Create a mysql database jnamed 'eae'

#### - Create .env (copy the .env.example). Edit this settings to match your environment settings and credentials:
###### DB_HOST=
###### DB_PORT=
###### DB_DATABASE=
###### DB_USERNAME=
###### DB_PASSWORD=

#### - Run `php artisan migrate --seed` to create and populate database tables.

#### - Serve the application with `php artisan serve`. You should see an URL in your terminal.

#### - Open that URL in a browser to check if everything is running smoothly. Access URL/api/jobs, you should see JSON data.

