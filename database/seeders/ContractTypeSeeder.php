<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\ContractType;

class ContractTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!ContractType::count()) {
            ContractType::create(['description' => 'Full Time']);
            ContractType::create(['description' => 'Part Time']);
            ContractType::create(['description' => 'Contract']);
        }
    }
}
