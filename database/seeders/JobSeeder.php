<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Job;
use App\Models\Company;
use Illuminate\Support\Facades\DB;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Job::count()) {
            Job::create([
                'company_id' => DB::table('companies')
                    ->select('id')
                    ->where('name', 'Photosnap')
                    ->first()
                    ->id,
                'new' => true,
                'featured' => true,
                'position' => 'Senior Frontend Developer',
                'role_id' => DB::table('roles')
                    ->select('id')
                    ->where('description', 'Frontend')
                    ->first()
                    ->id,
                'level_id' => DB::table('levels')
                    ->select('id')
                    ->where('description', 'Senior')
                    ->first()
                    ->id,
                'contract_type_id' => DB::table('contract_types')
                    ->select('id')
                    ->where('description', 'Full Time')
                    ->first()
                    ->id,
                'location' => 'USA Only'


            ]);
            Job::create([
                'company_id' => DB::table('companies')
                    ->select('id')
                    ->where('name', 'Manage')
                    ->first()
                    ->id,
                'new' => true,
                'featured' => true,
                'position' => 'Fullstack Developer',
                'role_id' => DB::table('roles')
                    ->select('id')
                    ->where('description', 'Fullstack')
                    ->first()
                    ->id,
                'level_id' => DB::table('levels')
                    ->select('id')
                    ->where('description', 'Midweight')
                    ->first()
                    ->id,
                'contract_type_id' => DB::table('contract_types')
                    ->select('id')
                    ->where('description', 'Part Time')
                    ->first()
                    ->id,
                'location' => 'Remote'

            ]);
            Job::create([
                'company_id' => DB::table('companies')
                    ->select('id')
                    ->where('name', 'Account')
                    ->first()
                    ->id,
                'new' => true,
                'featured' => false,
                'position' => 'Junior Frontend Developer',
                'role_id' => DB::table('roles')
                    ->select('id')
                    ->where('description', 'Frontend')
                    ->first()
                    ->id,
                'level_id' => DB::table('levels')
                    ->select('id')
                    ->where('description', 'Junior')
                    ->first()
                    ->id,
                'contract_type_id' => DB::table('contract_types')
                    ->select('id')
                    ->where('description', 'Part Time')
                    ->first()
                    ->id,
                'location' => 'USA Only'

            ]);

            Job::create([
                'company_id' => DB::table('companies')
                    ->select('id')
                    ->where('name', 'MyHome')
                    ->first()
                    ->id,
                'new' => false,
                'featured' => false,
                'position' => 'Junior Frontend Developer',
                'role_id' => DB::table('roles')
                    ->select('id')
                    ->where('description', 'Frontend')
                    ->first()
                    ->id,
                'level_id' => DB::table('levels')
                    ->select('id')
                    ->where('description', 'Junior')
                    ->first()
                    ->id,
                'contract_type_id' => DB::table('contract_types')
                    ->select('id')
                    ->where('description', 'Contract')
                    ->first()
                    ->id,
                'location' => 'USA Only'

            ]);


            Job::create([
                'company_id' => DB::table('companies')
                    ->select('id')
                    ->where('name', 'Loop Studios')
                    ->first()
                    ->id,
                'new' => false,
                'featured' => false,
                'position' => 'Software Engineer',
                'role_id' => DB::table('roles')
                    ->select('id')
                    ->where('description', 'Fullstack')
                    ->first()
                    ->id,
                'level_id' => DB::table('levels')
                    ->select('id')
                    ->where('description', 'Midweight')
                    ->first()
                    ->id,
                'contract_type_id' => DB::table('contract_types')
                    ->select('id')
                    ->where('description', 'Contract')
                    ->first()
                    ->id,
                'location' => 'Worldwide'

            ]);

            Job::create([
                'company_id' => DB::table('companies')
                    ->select('id')
                    ->where('name', 'Loop Studios')
                    ->first()
                    ->id,
                'new' => false,
                'featured' => false,
                'position' => 'Junior Backend Developer',
                'role_id' => DB::table('roles')
                    ->select('id')
                    ->where('description', 'Backend')
                    ->first()
                    ->id,
                'level_id' => DB::table('levels')
                    ->select('id')
                    ->where('description', 'Junior')
                    ->first()
                    ->id,
                'contract_type_id' => DB::table('contract_types')
                    ->select('id')
                    ->where('description', 'Full Time')
                    ->first()
                    ->id,
                'location' => 'UK Only'

            ]);

            Job::create([
                'company_id' => DB::table('companies')
                    ->select('id')
                    ->where('name', 'Shortly')
                    ->first()
                    ->id,
                'new' => false,
                'featured' => false,
                'position' => 'Junior Developer',
                'role_id' => DB::table('roles')
                    ->select('id')
                    ->where('description', 'Frontend')
                    ->first()
                    ->id,
                'level_id' => DB::table('levels')
                    ->select('id')
                    ->where('description', 'Junior')
                    ->first()
                    ->id,
                'contract_type_id' => DB::table('contract_types')
                    ->select('id')
                    ->where('description', 'Full Time')
                    ->first()
                    ->id,
                'location' => 'Worldwide'

            ]);

            Job::create([
                'company_id' => DB::table('companies')
                    ->select('id')
                    ->where('name', 'Insure')
                    ->first()
                    ->id,
                'new' => false,
                'featured' => false,
                'position' => 'Junior Frontend Developer',
                'role_id' => DB::table('roles')
                    ->select('id')
                    ->where('description', 'Frontend')
                    ->first()
                    ->id,
                'level_id' => DB::table('levels')
                    ->select('id')
                    ->where('description', 'Junior')
                    ->first()
                    ->id,
                'contract_type_id' => DB::table('contract_types')
                    ->select('id')
                    ->where('description', 'Full Time')
                    ->first()
                    ->id,
                'location' => 'USA Only'

            ]);

            Job::create([
                'company_id' => DB::table('companies')
                    ->select('id')
                    ->where('name', 'Manage')
                    ->first()
                    ->id,
                'new' => false,
                'featured' => false,
                'position' => 'Full Stack Engineer',
                'role_id' => DB::table('roles')
                    ->select('id')
                    ->where('description', 'Fullstack')
                    ->first()
                    ->id,
                'level_id' => DB::table('levels')
                    ->select('id')
                    ->where('description', 'Midweight')
                    ->first()
                    ->id,
                'contract_type_id' => DB::table('contract_types')
                    ->select('id')
                    ->where('description', 'Full Time')
                    ->first()
                    ->id,
                'location' => 'Worldwide'

            ]);

            Job::create([
                'company_id' => DB::table('companies')
                    ->select('id')
                    ->where('name', 'Photosnap')
                    ->first()
                    ->id,
                'new' => false,
                'featured' => false,
                'position' => 'Front-end Dev',
                'role_id' => DB::table('roles')
                    ->select('id')
                    ->where('description', 'Frontend')
                    ->first()
                    ->id,
                'level_id' => DB::table('levels')
                    ->select('id')
                    ->where('description', 'Junior')
                    ->first()
                    ->id,
                'contract_type_id' => DB::table('contract_types')
                    ->select('id')
                    ->where('description', 'Part Time')
                    ->first()
                    ->id,
                'location' => 'Worldwide'

            ]);
            
        }
    }
}
