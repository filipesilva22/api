<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\JobLanguage;

class JobLanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!JobLanguage::count()) {
            JobLanguage::create(['job_id' => 1, "language_id" => 1]);
            JobLanguage::create(['job_id' => 1, "language_id" => 2]);
            JobLanguage::create(['job_id' => 1, "language_id" => 3]);
            JobLanguage::create(['job_id' => 2, "language_id" => 4]);
            JobLanguage::create(['job_id' => 3, "language_id" => 3]);
            JobLanguage::create(['job_id' => 4, "language_id" => 2]);
            JobLanguage::create(['job_id' => 4, "language_id" => 3]);
            JobLanguage::create(['job_id' => 5, "language_id" => 3]);
            JobLanguage::create(['job_id' => 6, "language_id" => 5]);
            JobLanguage::create(['job_id' => 7, "language_id" => 1]);
            JobLanguage::create(['job_id' => 7, "language_id" => 3]);
            JobLanguage::create(['job_id' => 8, "language_id" => 3]);
            JobLanguage::create(['job_id' => 9, "language_id" => 3]);
            JobLanguage::create(['job_id' => 9, "language_id" => 4]);
            JobLanguage::create(['job_id' => 10, "language_id" => 3]);
        }
    }
}
