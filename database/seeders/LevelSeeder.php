<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Level;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Level::count()) {
            Level::create(['description' => 'Senior']);
            Level::create(['description' => 'Midweight']);
            Level::create(['description' => 'Junior']);
        }
    }
}
