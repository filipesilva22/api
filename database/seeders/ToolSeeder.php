<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Tool;

class ToolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Tool::count()) {
            Tool::create(['name' => 'React']);
            Tool::create(['name' => 'Sass']);
            Tool::create(['name' => 'Ruby']);
            Tool::create(['name' => 'RoR']);
            Tool::create(['name' => 'Vue']);
            Tool::create(['name' => 'Django']);
        }
    }
}
