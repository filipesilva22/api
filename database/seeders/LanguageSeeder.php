<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Language;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Language::count()) {
            Language::create(['name' => 'HTML']);
            Language::create(['name' => 'CSS']);
            Language::create(['name' => 'JavaScript']);
            Language::create(['name' => 'Python']);
            Language::create(['name' => 'Ruby']);
        }
    }
}
