<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Role::count()) {
            Role::create(['description' => 'Frontend']);
            Role::create(['description' => 'Fullstack']);
            Role::create(['description' => 'Backend']);
        }
    }
}
