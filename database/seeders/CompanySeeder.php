<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Company::count()) {
            Company::create(['name' => 'Photosnap', 'logo' => "./assets/images/photosnap.svg"]);
            Company::create(['name' => 'Manage', 'logo' => "./assets/images/manage.svg"]);
            Company::create(['name' => 'Account', 'logo' => "./assets/images/account.svg"]);
            Company::create(['name' => 'MyHome', 'logo' => "./assets/images/myhome.svg"]);
            Company::create(['name' => 'Loop Studios', 'logo' => "./assets/images/loop-studios.svg"]);
            Company::create(['name' => 'Shortly', 'logo' => "./assets/images/shortly.svg"]);
            Company::create(['name' => 'Insure', 'logo' => "./assets/images/insure.svg"]);
        }
    }
}
