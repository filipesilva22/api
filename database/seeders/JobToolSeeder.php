<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\JobTool;

class JobToolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!JobTool::count()) {
            JobTool::create(['job_id' => 2, "tool_id" => 1]);
            JobTool::create(['job_id' => 3, "tool_id" => 1]);
            JobTool::create(['job_id' => 3, "tool_id" => 2]);
            JobTool::create(['job_id' => 5, "tool_id" => 3]);
            JobTool::create(['job_id' => 5, "tool_id" => 2]);
            JobTool::create(['job_id' => 6, "tool_id" => 4]);
            JobTool::create(['job_id' => 7, "tool_id" => 2]);
            JobTool::create(['job_id' => 8, "tool_id" => 5]);
            JobTool::create(['job_id' => 8, "tool_id" => 2]);
            JobTool::create(['job_id' => 9, "tool_id" => 6]);
            JobTool::create(['job_id' => 10, "tool_id" => 1]);
            JobTool::create(['job_id' => 10, "tool_id" => 2]);
        }
    }
}
