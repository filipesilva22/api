<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CompanySeeder::class,
            RoleSeeder::class,
            LevelSeeder::class,
            ContractTypeSeeder::class,
            LanguageSeeder::class,
            ToolSeeder::class,
            JobSeeder::class,
            JobLanguageSeeder::class,
            JobToolSeeder::class
        ]);
    }
}
