<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    use HasFactory;

    /**
     * Get the role of the job.
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Get the role of the job.
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * Get the level required for the job.
     */
    public function level()
    {
        return $this->belongsTo(Level::class);
    }

    /**
     * Get the contract type of the job.
     */
    public function contractType()
    {
        return $this->belongsTo(ContractType::class);
    }

    /**
     * The languages required for the job.
     */
    public function languages()
    {
        return $this->belongsToMany(Language::class);
    }

    /**
     * The tools required for the job.
     */
    public function tools()
    {
        return $this->belongsToMany(Tool::class);
    }
}
