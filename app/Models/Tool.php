<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tool extends Model
{
    use HasFactory;
    
    /**
     * The jobs that require this tool.
     */
    public function jobs()
    {
        return $this->belongsToMany(Job::class);
    }
}
