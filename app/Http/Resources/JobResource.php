<?php

namespace App\Http\Resources;

use App\Models\Company;
use App\Models\ContractType;
use App\Models\Level;
use App\Models\Role;
use Illuminate\Http\Resources\Json\JsonResource;

class JobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $humanTime = $this->humanTiming(strtotime($this->created_at->format("Y-m-d H:i:s")));
        return [
            'id' => $this->id,
            'company' => $this->company->name,
            'logo' => $this->company->logo,
            'new' => $this->new == 1 ? true : false,
            'featured' => $this->featured == 1 ? true : false,
            'position' => $this->position,
            'role' => $this->role->description,
            'level' => $this->level->description,
            'postedAt' => $humanTime . ' ago',
            'contract' => $this->contractType->description,
            'location' => $this->location,
            'languages' => $this->languages->pluck('name'),
            'tools' => $this->tools->pluck('name')
        ];
    }

    function humanTiming ($time)
        {

            $time = time() - $time; // to get the time since that moment
            $time = ($time<1)? 1 : $time;
            $tokens = array (
                31536000 => 'y',
                2592000 => 'mo',
                604800 => 'w',
                86400 => 'd',
                3600 => 'h',
                60 => 'm',
                1 => 's'
            );

            foreach ($tokens as $unit => $text) {
                if ($time < $unit) continue;
                $numberOfUnits = floor($time / $unit);
                return $numberOfUnits.$text;
                /* return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':''); */
            }

        }
}
